module.exports = {
  plugins: {
    'postcss-import': {
      path: ['/src/css', '/src/fonts', '/src/scss'],
    },
    'postcss-preset-env': {},
    'cssnano': {},
  },
};
