### Prerequisites

Prerequisites: Node.js (^10.12.0, or >=12.0.0)

### Get started

```
  cd frontend-labs
  npm i
  npm start
```

### Put your files into /src folder
