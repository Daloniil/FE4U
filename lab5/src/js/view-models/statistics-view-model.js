import EventEmitter from '../event-emitter';

export default class StatisticsViewModel {
  constructor(dataService, teacherListViewModel) {
    this.dataService = dataService;
    this.teacherListViewModel = teacherListViewModel;

    this.pageNumber = 1;
    this.currentSortingOpts = {
      field: '',
      ascending: 1,
    };
    this.newPageEmitter = new EventEmitter();

    this.teacherListViewModel.dataUpdatedEmitter.subscribe(() => {
      this.newPageEmitter.emit();
    });
  }

  getCurrentPageData() {
    const sortedData = this.dataService.sortPeople(
      this.currentSortingOpts, { data: this.teacherListViewModel.data },
    );
    return sortedData.slice(10 * (this.pageNumber - 1), 10 * this.pageNumber);
  }

  setSortingField(sortingField) {
    if (this.currentSortingOpts.field === sortingField) {
      if (this.currentSortingOpts.ascending === -1) {
        this.currentSortingOpts = {
          field: '',
          ascending: 1,
        };
      } else {
        this.currentSortingOpts = {
          field: sortingField,
          ascending: -1,
        };
      }
    } else {
      this.currentSortingOpts = {
        field: sortingField,
        ascending: 1,
      };
    }
    this.newPageEmitter.emit(this.getCurrentPageData());
  }

  get totalPages() {
    return Math.ceil(this.teacherListViewModel.data.length / 10);
  }

  setPage(page) {
    this.pageNumber = page;
    this.newPageEmitter.emit(this.getCurrentPageData());
  }
}
