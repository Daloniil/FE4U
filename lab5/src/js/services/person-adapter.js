import {
  getPersonBirthDate,
  getPersonLocation,
  getPersonName,
  getPersonPicture, getRandomCourse, idToString,
} from '../utils/people-utils';
import { capitalize } from '../utils/str-utils';

export default class PersonAdapter {
  constructor(person) {
    this.person = person;
  }

  forward() {
    const name = getPersonName(this.person);
    const location = getPersonLocation(this.person);
    const birthDate = getPersonBirthDate(this.person);
    const picture = getPersonPicture(this.person);
    return {
      gender: capitalize(this.person.gender),
      title: capitalize(name.title),
      full_name: name.fullName,
      city: capitalize(location.city),
      state: capitalize(location.state),
      country: capitalize(location.country),
      postcode: location.postcode,
      coordinates: location.coordinates,
      timezone: location.timezone,
      email: this.person.email ? this.person.email : null,
      b_date: birthDate.date,
      age: birthDate.age,
      phone: this.person.phone ? this.person.phone : null,
      picture_large: picture.large,
      picture_thumbnail: picture.thumbnail,
      id: idToString(this.person.id),
      favorite: this.person.favorite === true,
      bg_color: this.person.bg_color ? this.person.bg_color : '#fff',
      note: capitalize(this.person.note ? this.person.note : ''),
      course: this.person.course ? capitalize(this.person.course) : getRandomCourse(),
    };
  }
}
