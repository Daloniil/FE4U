import _ from 'lodash';
import {
  getPersonBirthDate,
  getPersonLocation,
  getPersonName,
  getPersonPicture,
  getRandomCourse,
  idToString,
} from '../utils/people-utils';

export default class PersonAdapter {
  constructor(person) {
    this.person = person;
  }

  forward() {
    const name = getPersonName(this.person);
    const location = getPersonLocation(this.person);
    const birthDate = getPersonBirthDate(this.person);
    const picture = getPersonPicture(this.person);
    return {
      gender: _.capitalize(this.person.gender),
      title: _.capitalize(name.title),
      full_name: name.fullName,
      city: _.capitalize(location.city),
      state: _.capitalize(location.state),
      country: _.capitalize(location.country),
      postcode: location.postcode,
      coordinates: location.coordinates,
      timezone: location.timezone,
      email: this.person.email ? this.person.email : null,
      b_date: birthDate.date,
      age: birthDate.age,
      phone: this.person.phone ? this.person.phone : null,
      picture_large: picture.large,
      picture_thumbnail: picture.thumbnail,
      id: idToString(this.person.id),
      favorite: this.person.favorite === true,
      bg_color: this.person.bg_color ? this.person.bg_color : '#fff',
      note: _.capitalize(this.person.note ? this.person.note : ''),
      course: this.person.course ? _.capitalize(this.person.course) : getRandomCourse(),
    };
  }
}
