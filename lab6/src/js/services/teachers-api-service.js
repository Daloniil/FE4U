import PersonAdapter from './person-adapter';

const apiLink = 'https://randomuser.me/api';

export default class TeachersApiService {
  // eslint-disable-next-line class-methods-use-this
  async fetchTeachers(amount) {
    const response = await fetch(`${apiLink}/?results=${amount}`);
    const json = await response.json();
    return json.results.map((person) => new PersonAdapter(person).forward());
  }

  // eslint-disable-next-line class-methods-use-this
  async saveTeacher(teacher) {
    const response = await fetch('http://localhost:3000/teacher', {
      method: 'POST',
      body: JSON.stringify(teacher),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    if (response.status === 201) {
      return {
        isOk: true,
        teacher: await response.json(),
      };
    }
    return {
      isOk: false,
      errors: await response.json(),
    };
  }
}
