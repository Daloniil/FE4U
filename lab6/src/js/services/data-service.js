import _ from 'lodash';
import { search } from '../data';
import EventEmitter from '../event-emitter';
import { courses } from '../utils/people-utils';

export default class DataService {
  constructor(apiService) {
    this.apiService = apiService;
    this.data = [];
    this.dataUpdatedEmitter = new EventEmitter();
    this.newPageLoadedEmitter = new EventEmitter();
    this.favoritesUpdatedEmitter = new EventEmitter();
    this.countries = [
      'Australia',
      'Canada',
      'Denmark',
      'Finland',
      'France',
      'Germany',
      'Iran',
      'Ireland',
      'Netherlands',
      'New Zealand',
      'Norway',
      'Spain',
      'Switzerland',
      'Turkey',
      'Ukraine',
      'United States',
    ];
    this.courses = courses;
  }

  async initData() {
    this.data = await this.apiService.fetchTeachers(50);
    this.newPageLoadedEmitter.emit(this.data);
  }

  async loadNextPage() {
    const response = await this.apiService.fetchTeachers(10);
    this.data = [...this.data, ...response];
    this.newPageLoadedEmitter.emit(response);
  }

  async addTeacher(teacher) {
    const response = await this.apiService.saveTeacher(teacher);
    if (response.isOk) {
      this.data = [...this.data, response.teacher];
      this.dataUpdatedEmitter.emit();
      return null;
    }
    return response.errors;
  }

  getFavorites() {
    return this.data.filter((person) => person.favorite);
  }

  toggleFavorite(name) {
    const person = this.data.find((p) => p.full_name === name);
    person.favorite = !person.favorite;
    this.favoritesUpdatedEmitter.emit(this.getFavorites());
  }

  filterPeople(filter) {
    let filteredData = this.data;
    if (filter.country) {
      filteredData = _.filter(filteredData, (person) => person.country === filter.country);
    }
    if (filter.age) {
      filteredData = _.filter(
        filteredData,
        (person) => _.inRange(person.age, filter.age.min || 0, filter.age.max || 120),
      );
    }
    if (filter.gender) {
      filteredData = _.filter(filteredData, (person) => person.gender === filter.gender);
    }
    if (typeof filter.onlyWithPhoto === 'boolean') {
      filteredData = _.filter(
        filteredData,
        (person) => !!person.picture_large === filter.onlyWithPhoto,
      );
    }
    if (typeof filter.favorite === 'boolean') {
      filteredData = _.filter(filteredData, (person) => person.favorite === filter.favorite);
    }
    return filteredData;
  }

  sortPeople(opts, { data = this.data }) {
    let sortingField;
    if (typeof opts === 'string') {
      if (opts === '') {
        return [...data];
      }
      sortingField = opts;
    } else if (typeof opts === 'object') {
      if (opts.field === '') {
        return [...data];
      }
      sortingField = opts.field;
    }
    let sorted = _.sortBy(this.data, sortingField);
    if (opts.ascending === false) {
      sorted = _.reverse(sorted);
    }
    return sorted;
  }

  search(query) {
    return search(this.data, query);
  }
}
