import _ from 'lodash';
import EventEmitter from '../event-emitter';

export default class StatisticsViewModel {
  constructor(dataService, teacherListViewModel) {
    this.dataService = dataService;
    this.teacherListViewModel = teacherListViewModel;

    this.countingField = 'course';
    this.newPageEmitter = new EventEmitter();
    this.newFieldEmitter = new EventEmitter();

    this.countingFields = {
      specialities: 'course',
      nationality: 'country',
      age: 'age',
      gender: 'gender',
    };

    this.teacherListViewModel.dataUpdatedEmitter.subscribe(() => {
      this.newPageEmitter.emit();
    });
  }

  getStatistics() {
    return _.countBy(this.teacherListViewModel.data, (person) => person[this.countingField]);
  }

  setField(newField) {
    this.countingField = newField;
    this.newFieldEmitter.emit();
  }
}
