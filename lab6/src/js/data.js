import _ from 'lodash';

require('../scss/index.scss');

export const peopleComparator = (field, ascending) => (a, b) => {
  if (a[field] > b[field]) {
    return ascending;
  }
  if (a[field] < b[field]) {
    return -ascending;
  }
  return 0;
};

export function search(data, query) {
  if (!Number.isNaN(Number(query))) {
    return _.filter(data, (person) => person.age === Number(query));
  }
  return _.filter(
    data,
    (person) => person.full_name.includes(query) || person.note.includes(query),
  );
}

export function getPercentageForSearch(data, query) {
  const searchResults = search(data, query);
  return (searchResults.length / data.length) * 100;
}
