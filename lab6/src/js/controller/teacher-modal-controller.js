import L from 'leaflet';

export default class TeacherModalController {
  constructor(viewModel) {
    this.viewModel = viewModel;
    this.map = null;
  }

  connect({
    toggleFavoriteButton,
    toggleFavoriteButtonMobile,
    toggleMapButton,
    mapDiv,
    teacher,
  }) {
    this.toggleFavoriteBtn = document.getElementById(toggleFavoriteButton);
    this.toggleFavoriteBtn.onclick = () => {
      this.viewModel.dataService.toggleFavorite(teacher.full_name);
      this.toggleFavoriteBtn.innerText = teacher.favorite ? '★' : '☆';
    };
    this.toggleFavoriteBtnMobile = document.getElementById(toggleFavoriteButtonMobile);
    this.toggleFavoriteBtnMobile.onclick = () => {
      this.viewModel.dataService.toggleFavorite(teacher.full_name);
      this.toggleFavoriteBtnMobile.innerText = teacher.favorite ? '★ Delete from favorites' : '☆ Add to favorites';
    };
    this.toggleMapBtn = document.getElementById(toggleMapButton);
    this.mapDiv = document.getElementById(mapDiv);
    if (this.toggleMapBtn && this.mapDiv) {
      this.toggleMapBtn.onclick = () => {
        const isMapShown = this.mapDiv.style.display !== '' && this.mapDiv.style.display !== 'none';
        this.mapDiv.style.display = isMapShown ? 'none' : 'block';
        this.map.invalidateSize();
      };
      this.map = L.map('map', {
        center: [teacher.coordinates.latitude, teacher.coordinates.longitude],
        zoom: 13,
      });
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      })
        .addTo(this.map);
      L.marker([teacher.coordinates.latitude, teacher.coordinates.longitude])
        .addTo(this.map);
    }
  }
}
