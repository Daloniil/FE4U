import Chart from 'chart.js/auto';
import _ from 'lodash';

export default class StatisticsController {
  constructor(viewModel) {
    this.viewModel = viewModel;
    this.viewModel.newPageEmitter.subscribe(() => {
      this.renderStatisticsChart();
    });
    this.viewModel.newFieldEmitter.subscribe(() => {
      this.renderStatisticsChart();
    });
  }

  connect({
    statsCanvas,
    fieldSelectBox,
  }) {
    this.attachElements({
      statsCanvas,
      fieldSelectBox,
    });
    this.renderStatisticsChart();
  }

  attachElements({
    statsCanvas,
    fieldSelectBox,
  }) {
    this.statsCanvas = document.getElementById(statsCanvas);
    this.fieldSelectBox = document.getElementById(fieldSelectBox);

    this.renderFieldSelect();

    this.chart = new Chart(this.statsCanvas, {
      type: 'pie',
      data: {},
    });
  }

  renderFieldSelect() {
    this.fieldSelectBox.innerHTML = '';
    _.map(this.viewModel.countingFields, (field, displayName) => {
      const option = document.createElement('option');
      option.value = field;
      option.innerText = displayName;
      return option;
    }).forEach((option) => this.fieldSelectBox.appendChild(option));
    this.fieldSelectBox.onchange = (ev) => {
      this.viewModel.setField(ev.target.value);
    };
  }

  renderStatisticsChart() {
    const statsData = this.viewModel.getStatistics();
    const labels = _.keys(statsData);
    this.chart.data = {
      labels,
      datasets: [{
        label: 'Specialities',
        backgroundColor: [
          'rgb(255, 99, 132)',
          'rgb(54, 162, 235)',
          'rgb(255, 205, 86)',
          'rgb(123,255,86)',
          'rgb(86,255,255)',
          'rgb(255,176,86)',
          'rgb(232,86,255)',
        ],
        data: _.values(statsData),
      }],
    };
    this.chart.update();
  }
}
