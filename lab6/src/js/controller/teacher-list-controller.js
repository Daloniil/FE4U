import TeachersBaseController from './teachers-base-controller';

function createSkeleton() {
  const skeletonDiv = document.createElement('div');
  skeletonDiv.classList.add('teacher-compact-info', 'teacher-skeleton');
  skeletonDiv.innerHTML = `
    <div class="teacher-avatar-skeleton"></div>
    <p class="teacher-skeleton__name"></p>
    <p class="teacher-skeleton__specialty"></p>
    <p class="teacher-skeleton__nationality"></p>
  `;
  return skeletonDiv;
}

export default class TeacherListController extends TeachersBaseController {
  constructor({
    viewModel,
    modalController,
  }) {
    super(viewModel, modalController);
    this.viewModel = viewModel;
    this.viewModel.dataUpdatedEmitter.subscribe(() => {
      this.renderTeachersList();
    });
    this.viewModel.dataService.dataUpdatedEmitter.subscribe(() => {
      this.renderCountriesSelectBox();
    });
    this.viewModel.newPageLoadedEmitter.subscribe(() => {
      this.replaceSkeletonsWithData();
    });
  }

  connect({
    teachersListDiv,
    searchForm,
    ageFilterInput,
    countryFilterInput,
    sexFilterInput,
    withPhotoFilterInput,
    onlyFavoritesFilterInput,
    loadMoreButton,
  }) {
    this.attachElements({
      teachersListDiv,
      countryFilterInput,
      searchForm,
      ageFilterInput,
      sexFilterInput,
      withPhotoFilterInput,
      onlyFavoritesFilterInput,
      loadMoreButton,
    });

    this.renderCountriesSelectBox();
  }

  replaceSkeletonsWithData() {
    const skeletons = this.teachersList.querySelectorAll('.teacher-skeleton');
    skeletons.forEach((skeleton) => this.teachersList.removeChild(skeleton));
    this.renderTeachersList();
  }

  attachElements({
    teachersListDiv,
    countryFilterInput,
    searchForm,
    ageFilterInput,
    sexFilterInput,
    withPhotoFilterInput,
    onlyFavoritesFilterInput,
    loadMoreButton,
  }) {
    this.teachersList = document.getElementById(teachersListDiv);
    this.countrySelectBox = document.getElementById(countryFilterInput);

    document.getElementById(searchForm).onsubmit = (ev) => {
      ev.preventDefault();
      const query = ev.target.elements.namedItem('query').value;
      this.viewModel.onSearch(query);
    };
    document.getElementById(ageFilterInput).onchange = (ev) => {
      this.viewModel.onAgeFilterChanged(ev.target.value);
    };
    document.getElementById(countryFilterInput).onchange = (ev) => {
      this.viewModel.onCountryFilterChanged(ev.target.value);
    };
    document.getElementById(sexFilterInput).onchange = (ev) => {
      this.viewModel.onSexFilterChanged(ev.target.value);
    };
    document.getElementById(withPhotoFilterInput).onchange = (ev) => {
      this.viewModel.onOnlyWithPhotoFilterChanged(ev.target.checked);
    };
    document.getElementById(onlyFavoritesFilterInput).onchange = (ev) => {
      this.viewModel.onOnlyFavoritesFilterChanged(ev.target.checked);
    };
    document.getElementById(loadMoreButton).onclick = () => {
      this.loadMoreTeachers();
    };
  }

  renderTeachersList() {
    this.teachersList.innerHTML = '';
    this.viewModel.data
      .map((teacher) => super.createTeacherShortInfo(teacher))
      .forEach((div) => this.teachersList.appendChild(div));
  }

  renderCountriesSelectBox() {
    this.countrySelectBox.innerHTML = '';
    const { countries } = this.viewModel.dataService;
    const anyOption = document.createElement('option');
    anyOption.innerText = 'Any';
    this.countrySelectBox.appendChild(anyOption);
    countries.forEach((country) => {
      const countryOption = document.createElement('option');
      countryOption.innerText = country;
      if (country === this.viewModel.filters.country) {
        countryOption.selected = true;
      }
      this.countrySelectBox.appendChild(countryOption);
    });
  }

  async loadMoreTeachers() {
    this.addSkeletons(10);
    await this.viewModel.loadMoreTeachers();
  }

  addSkeletons(amount) {
    for (let i = 0; i < amount; i += 1) {
      const skeleton = createSkeleton();
      this.teachersList.appendChild(skeleton);
    }
  }
}
