const jsonServer = require('json-server');
const dayjs = require('dayjs');
const ValidatorService = require('./validator-service');

const server = jsonServer.create();
server.use(jsonServer.defaults());
server.use(jsonServer.bodyParser);
const validator = new ValidatorService();

server.post('/teacher', (req, res) => {
  const teacher = req.body;
  const validatingErrors = validator.validateTeacher(teacher);
  if (validatingErrors.length === 0) {
    teacher.age = dayjs().diff(dayjs(teacher.b_date), 'year');
    res.status(201)
      .json(teacher);
  } else {
    res.status(400).json(validatingErrors);
  }
});

server.listen(3000);
