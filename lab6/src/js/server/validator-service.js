const dayjs = require('dayjs');

function isUpperCase(str) {
  return str.toUpperCase() === str;
}

function validatePerson(person, phoneRegex) {
  const ageIsNumeric = person.age === null || typeof person.age === 'number';
  const fullNameIsValid = typeof person.full_name === 'string' && isUpperCase(person.full_name.charAt(0));
  const genderIsValid = typeof person.gender === 'string' && isUpperCase(person.gender.charAt(0));
  const noteIsValid = typeof person.note === 'string' && (person.note.length === 0 || isUpperCase(person.note.charAt(0)));
  const stateIsValid = !person.state || (typeof person.state === 'string' && isUpperCase(person.state.charAt(0)));
  const countryIsValid = person.country === null || (typeof person.country === 'string' && isUpperCase(person.country.charAt(0)));
  const birthdayIsValid = person.b_date !== null && dayjs(person.b_date) && dayjs(person.b_date)
    .isBefore(dayjs());
  const phoneIsValid = person.phone === null || phoneRegex.test(person.phone);
  const emailIsValid = person.email === null || (typeof person.email === 'string' && person.email.includes('@'));
  const errors = [];
  if (!ageIsNumeric) {
    errors.push('Invalid age');
  }
  if (!fullNameIsValid) {
    errors.push('Invalid name');
  }
  if (!genderIsValid) {
    errors.push('Invalid sex');
  }
  if (!noteIsValid) {
    errors.push('Invalid comment');
  }
  if (!stateIsValid) {
    errors.push('Invalid state');
  }
  if (!countryIsValid) {
    errors.push('Invalid country');
  }
  if (!birthdayIsValid) {
    errors.push('Invalid date of birth!');
  }
  if (!phoneIsValid) {
    errors.push('Invalid phone number');
  }
  if (!emailIsValid) {
    errors.push('Invalid email');
  }
  return errors;
}

class ValidatorService {
  constructor() {
    this.countries = {
      australia: /^\+61 ?\d ?\d{4} ?\d{4}$/,
      canada: /^\+1\d{3}-?\d{3}-?\d{4}$/,
      denmark: /^\+45 ?\d{2} ?\d{2} ?\d{2} ?\d{2}$/,
      finland: /^\+358 ?\d{3}-?\d{4}-?\d{3}$/,
      france: /^\+33 ?\d ?\d{2} ?\d{2} ?\d{2} ?\d{2}$/,
      germany: /^\+49 ?\d{3} ?\d{7}$/,
      iran: /^\+98 ?9\d{2} ?\d{3} ?\d{4}$/,
      ireland: /^\+353 ?\d ?\d{3} ?\d{4}$/,
      netherlands: /^\+31 ?\d{3}-?\d{4}-?\d{2}$/,
      new_zealand: /^\+64 ?\d ?\d{4} ?\d{4}$/,
      norway: /^\+47 ?\d{8} ?\d{8}$/,
      spain: /^\++34 ?\d{2} ?\d{3} ?\d{3}$/,
      switzerland: /^\+41 ?\d{2} \d{3} \d{2} \d{2}$/,
      turkey: /^\+90 ?\d{3} \d{3} \d{4}$/,
      ukraine: /^\+380\d{9}$/,
      united_states: /^\+1 ?((\(\d{3}\))|(\d{3})) ?\d{3}-?\d{4}$/,
    };
  }

  validateTeacher(teacher) {
    const countryName = teacher.country.toLowerCase()
      .replace(' ', '_');
    if (this.countries[countryName] === undefined) {
      return ['invalid country'];
    }
    return validatePerson(teacher, this.countries[countryName]);
  }
}

module.exports = ValidatorService;
